\documentclass[12pt,a4paper]{scrreprt}
\usepackage[margin=1.1in]{geometry}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}

% Police
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\renewcommand*\rmdefault{bch} % Charter
\addtokomafont{disposition}{\rmfamily} % même police pour les titres

% Configuration des liens (table des matières) + pdf metadata
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  citecolor=black,
  filecolor=black,
  linkcolor=black,
  urlcolor=black,
  %%%
  pdfauthor={Antoine Roulin, Nicolas Darbois},
  pdftitle={Design Document}
}

% Configuration du header des pages
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Software Engineering 2}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\rhead{
  \itshape\ifnum\value{chapter}>0 \thechapter\ \leftmark\fi
}

\newcommand{\trav}{Travlendar\textsuperscript+}

\begin{document}

\begin{titlepage}
  \centering
  \vspace{5cm}
  {\scshape\LARGE Software Engineering 2\par}
  \vspace{1cm}
  {\scshape\Large \trav\par}
  \vspace{1.5cm}
  {\huge\bfseries Design Document \par}
  \vspace{2cm}
  {\Large \textsc{Nicolas Darbois, Antoine Roulin}\par}
  \vfill
  
  Supervised by\par
  Mr.~\textsc{Rossi}
  \\~
  \\~
  {\large \today\par}
\end{titlepage}

\tableofcontents

\chapter{Introduction}

\section{Purpose}

This document aims to provide the development team an overall guidance to build the system \trav{} regarding:

\begin{itemize}
\item high level architecture
\item design patterns to use
\item components nature and interfaces between them
\item behaviour at runtime and maintenance
\end{itemize}

It will also follow to the requirements previously established in the RASD. Eventually, a planning describing each integration step and testing phase will also be provided.

\section{Scope}

This project aims to provide flexible and fully-featured calendar support that considers the travel time between meetings. Travlendar+ should support a multitude of travel means, including walking, biking, public transportation, and driving.
A particular user may globally activate or deactivate each travel means (e.g., a user who does not own a bicycle would deactivate biking).
A user should also be able to provide reasonable constraints on different travel means (e.g., walking distances should be less than a given distance, or public transportation should not be used after a given time of day). When a user interacts with Travlendar, the application should:
\begin{itemize}
\item compute travel times between meetings (or proposed meetings) and prevent scheduling meetings whose start/end times conflict as a result of inability to travel
  
\item explicitly but subtly add travel time to the calendar between meetings, reserving the required amount of time on the calendar, and suggesting the best option of travel between each meeting.
  
\item create overall compatible schedules on a day-basis, considering that a user who does not leave the house with his bicycle or car in the morning cannot use it to move between meetings during the day.
\end{itemize}

On a given day, a user's car (or bicycle) should start and end at the user's home.
The implementation of Travlendar+ should consider different types of users, including people who work on the road, traveling from meeting to meeting throughout the day as well as busy parents shuttling kids to and from.

Additional features could be envisioned, a user could specify a flexible “lunch” or other breaks that have to be scheduled and customizable.
The calendar could be made dynamic to consider the weather forecast, it could consider the cost of each transportation means, or it could provide users more substantial ability to provide preferences related to transportation modes.

\section{Definitions, acronyms, abbreviations}

\subsection{Definitions}

\subsection{Acronyms}

\begin{itemize}
\item API: Application Programming Interface
\item NLP: Natural Language Processing
\end{itemize}

\subsection{Abbreviations}

\section{Revision history}

\begin{itemize}
\item 26/11/2017 --- 1.0
\end{itemize}

\section{Reference documents}

\section{Document structure}

\subsection{Introduction}

This section introduces the design document. It explains the utility of the project, text conventions and the framework of the document.

\subsection{Architectural design}

This section illustrates the main components of the system and the relationships between them, providing information about their operating principles and deployment. This section will also focus on the main architectural styles and patterns adopted in the design of the system.

\subsection{Algorithm design}

This section describes the most critical parts of the platform in terms of algorithms. Java-like code is used to present the most important algorithmic aspects, which will be described without the least relevant implementation details.

\subsection{User interface design}

The designed interface will pe described using mockups views and a user experience flowchart.

\subsection{Requirements tracability}

This section will come back to the requirements established in the RASD document. We will see how they guided us during the design process of the system, and if we respected them.

\chapter{Architectural design}

\section{Overview}

\begin{itemize}
	\item  Component view gives a global view of the logical components of the application and how they communicate
	\item  Deployment view describes the tier division of the system and shows the components that must be deployed in order to run the applications properly
	\item  Runtime view provides a series of examples of how components actually communicate with each other with the use of Sequence Diagrams
	\item Component interfaces focuses on the interfaces between the modules of the system
\end{itemize}

\section{Component view}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{componentDiagram.png}
  \caption{Component diagram}
\end{figure}

\section{Deployment view}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{DeploymentDiagram1.png}
  \caption{Deployment diagram}
\end{figure}

\section{Runtime view}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{Personal_Data_Management.png}
  \caption{Personal data management}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{AppointmentManagement.png}
  \caption{Appointment management}
\end{figure}

\section{Component interfaces}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{componentInterface.png}
  \caption{Component interfaces diagram}
\end{figure}

\section{Architectural styles and patterns}

We will adopt a client/server architecture to serve the application from a server to the users’ web browser. The HTML views will be rendered on the server in order to minimize the need for client-side logic in Javascript. Only elements that are interactive by nature will be rendered client-side, like the calendar view. It will guarantee optimal performance and responsiveness even on low-powered devices like smartphones and tablets. It will also greatly decrease the size of data sent from the server to the client, because we will basically serve only static content. Furthermore, once some of the content like the CSS stylesheets have been loaded and cached, the load times for the other pages will be decreased.

The API calls will also happen on the server side, and the results will be stored in order to display consistent informations to the user. Those results will have to be actualized on a regular basis in order to take into account events like strikes or bad weather conditions. Because we expect no business logic to be handled only client-side, fallbacks will have to be provided and every input will have to be validated server-side, whether controls happened on the client or not. For instance, considering form validation, we won’t blindly trust data incoming from the client but expect that maybe the form was not validated by some Javascript controls.

The main advantage of client-side applications is that after the first request, which can take some time in order to transmit all the application structure, only data is transmitted by the server (JSON-formatted for example). But the time taken to establish the connection between the client and the server is actually what matters in most cases, not the length of the data transmitted. That is why we chose to directly transmit rendered views and not data to be rendered on the client, which is expensive in mobile use cases. Our views will also not be that big, so the difference in size will not be that important compared to the difference in time taken to display the final view.

It will also allow us not to use a framework for client-side development, because of the small needs we have. The Javascript needed will be directly embedded in the HTML templates if it is small enough to avoid unnecessary requests. We still consider using some kind of preprocessor to minify the static assets sent, like CSS or Javascript. In some cases those preprocessors can also allow for more functionalities like LESS for CSS or Typescript for Javascript. But because the scope of the project is not that big, we will begin the development without such tools which would add steps to the deployment procedure.

\chapter{Algorithm design}

\section{Events overlapping}

A simple formula will be used to determine if events overlap. Given two events e1 and e2, knowing for each the time at which they start and end, we can formulate the following predicate: e1 and e2 overlap if the starting time of e1 is before the ending time of e2, and the ending time of e1 is after the starting time of e2.

\begin{verbatim}
overlap(e1, e2) = (e1.from < e2.to) && (e1.to > e2.from)
\end{verbatim}

To take into account the travel time, once a travel means has been chosen and validated by the user, we can subtract the duration of the travel to the location of e1 from the starting point of e1, and do the same with e2.

\begin{verbatim}
overlap(e1, e2) =
    (e1.from - e1.travel.duration < e2.to) &&
    (e1.to > e2.from - e2.travel.duration)
\end{verbatim}

\section{Authentication and login}

Our authentication system will be based on special tokens sent to the user via mail, it allows fewer potential security breaches because the identification will be ensured by a third-party platform like Gmail. There won’t be any password saved in our database.
A token is a random sequence of characters.
This token will be encrypted. Once encrypted, the hash of the token will be saved in our database to confirm the registration or the reconnection.

To understand a bit more about how it is done, you can take a look at the diagram we made to summarize the actions done for the authentication.
These actions are performed when the user tries to access any pages requiring to be logged in our system.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{Connexion_utilisateurs.png}
  \caption{Authentication architecture}
\end{figure}


\section{Natural language event creation}

The user will be able to quickly add new events with information using an input text bar. The basic usage will be just typing the title of the event, like “Meeting about Travlendar+”. But the bar will also handle different kind of information like the location of the meeting and the participants. For instance “Meeting about Travlendar+ with Nicolas” will populate the new event form with not only the title “Meeting about Travlendar+” but also two participants: the user and another named Nicolas. The location is determined with keywords like “at”. The logic will not be incredibly complex, for a few reasons. First, it is not the main focus of the project is not NLP, it would take too much time and effort to develop a full-featured input parser. The main advantage of a simple and naive approach is also that its set of rules used for parsing input can be “learned” intuitively by the users. Indeed, because it will handle only some informations, expecting a certain format and using a small fixed set of rules, the parse process will be predictable and consistent.

Therefore we produced the following grammar under the Backus-Naur Form, which takes into account the fact that users have interest in typing correctly formed input. This means a few edge cases may be incorrect but still valid in regard to the grammar.

\begin{lstlisting}
input: title infos*
infos: time | date | place | members

title: title_bit+
title_bit: STRING

time: from_to | at_length
from_to: 'from' hour 'to' hour
at_length: 'at' hour 'during' length_time
length_time: hour_digit time_unit
time_unit: ('hours' | 'minutes')
hour: (hour_digit day_indicator) | hour_digit ':' hour_digit
day_indicator: morning | afternoon
morning: 'am' | 'AM'
afternoon: 'pm' | 'PM'
hour_digit: INTEGER

date: 'in' qty ('days' | 'months') | relative_date
relative_date: 'next' ('week' | 'month') | 'tomorrow'
qty: INTEGER

place: 'at' location_bit+
location_bit: STRING

members: 'with' name other_member*
name: name_bit+
name_bit: STRING
other_member: ('and' | '&' | ',') name other_member*
\end{lstlisting}

\chapter{User interface design}

We present here the conclusions of our design process of the user interface. This process started from a user experience point of view, only to come later to considerations such as style and aesthetics. That’s why we will begin by first presenting the flowchart of a typical user. We will then move on the implementation of the views needed by the system.

\section{User experience flowchart}

This flowchart describes the interactions possible for a user at every step of a normal usage of the system. The yellow squares are the pages among which he can navigate by making actions, represented in blue squares. Complementary informations about he views or actions are also depicted in gray squares.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{UX_Flowchart.png}
  \caption{UX Flowchart}
\end{figure}

We omitted pages such as help or the settings, which one can get to from any step of the system. It results in an intentionnally consise flowwchart. The \texttt{calendar} view will try to present an overall view of the events to come, while not becoming too dense visually. The \texttt{event details} will permit the user to get more information if he needs to. Finally, the new event form will be clearly separated and accessible at any time by the quick event creation bar. The goal was to slow the least possible the user in its workflow while presenting him useful infromations at a glance without visual overload. 

\section{Views}

Now that we established the flow of the interactions of the user with our system, we will show here what form they will take. Instead of making mockups in Photoshop or Sketch, HTML and CSS were directly used to implement the views. The starting point was for each the information needed to be displayed and the interactions to be expected on those informations. A consistent style was also adopted, and many components of the pages share the same styling properties. To provide a brief overview of the static pages, we include here for each view two screenshots: one simulating a laptop screen and one simulating a smartphone screen. The sources are available in the project repository, under the \texttt{mockup/} folder. They will be used during the development phase of the project, only to add the logic needed.

\subsection{Calendar view}

\begin{figure}[p]
  \centering
  \includegraphics[scale=0.5, angle=90]{view_calendar_month.png}
  \caption{Monthly calendar view}
  \label{fig:calView}
\end{figure}

We can see here (figure \ref{fig:calView}) the first and default view of the application, the monthly calendar view. On wide screens which allow it two months are displayed instead of one to provide more informations. Non-working days are displayed in a darker color to separate them from normal days. The selected day is highlighted in a more vibrant color. The current day (here November 8\textsuperscript{th}) is also displayed in a different color. Small indicators (red crosses) signal the presence of events during a day.

Selecting a day updates the content of a daily planner located beneath the calendar (displayed separatly because of the lack of space):

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{view_events_day.png}
  \caption{Events list}
\end{figure}

The switch over the calendar allows the user to display a recap of the events to come during the current working week:

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{view_calendar_week.png}
  \caption{Weekly calendar view}
\end{figure}

Once again are highlighted the selected day, the current day and the days conatining no events. Compared to the monthly view, the titles of the events are displayed.

\subsection{New event form}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{view_new.png}
  \caption{New event form}
\end{figure}

\subsection{Event details}

The role of this view is to provide as many informations about any given event as possible. Transport options are also displayed, and the prefferred one which will be taken into account for the scheduling is highlighted.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{view_details.png}
  \caption{Event details page}
\end{figure}

\subsection{Mobile}

One of the advantages of using HTML and CSS to produce the mockups is that they can easily be adapted for mobile use on smaller screens. Recent techniques such as Flexbox and CSS Grid have been used ---  which is now safe thanks to their rapid adoption by most modern browsers; those techniques further facilitate the implementation of a responsive design.

\begin{figure}
  \centering
  \includegraphics[width=.4\linewidth]{view_calendar_month_mobile.png}
  \includegraphics[width=.4\linewidth]{view_calendar_week_mobile.png}
  \caption{Calendar views adapt to smaller viewports}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=.4\linewidth]{view_new_mobile.png}
  \includegraphics[width=.4\linewidth]{view_details_mobile.png}
  \caption{Other pages are responsive too}
\end{figure}

\chapter{Requirements traceability}

\begin{itemize}
\item G1 The system should allow new users to register and login into our platform: R1

User Services:
	\begin{itemize}
		\item Account manager
	\end{itemize}
	
\item G2 The system should allow users to modify their personal data: R2

User Services:
	\begin{itemize}
		\item Account manager
	\end{itemize}

\item G3 The users should be allowed to manage their appointments: R3

User Services:
	\begin{itemize}
		\item Appointment manager
	\end{itemize}

\item G4 The users should be allowed to access all the itinerary informations: R4

User Services:
	\begin{itemize}
		\item Itinerary manager
	\end{itemize}
\end{itemize}

\chapter{Implementation, integration and test plan}


\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{timetable.png}
  \caption{Project timetable}
\end{figure}

\chapter{References}

Here we include a list of resources used during the preparation of this document.
They helped us to design the shape we want this project to take.

\begin{itemize}
\item \url{https://www.uml-diagrams.org/}
\item \url{https://www.ibm.com/developerworks/websphere/library/techarticles/0306_perks/perks2.html}
\end{itemize}

\chapter{Colophon}

\section{Tools}

\begin{itemize}
\item Google Drive to gather references
\item Google Docs to draft the document
\item \LaTeX~to typeset the document and produce the final pdf file
\item StarUML to produce diagrams
\item Draw.io to produce flowcharts
\item Git and GitHub to share source code and maintain a central repository
\end{itemize}

\section{Time spent}

The following tables give an indication of the time spent working on the present document, but do not include time spent on research or brainstorming.

\subsection{Antoine Roulin}

\begin{center}
  \begin{tabular}{ r l r }
    Date & Task & Hours \\
    \hline
    05/11 & Defining document structure & 2 \\
    08/11 & Static mockup & 4\\
    10/11 & Responsive mockup & 1\\
    14/11 & Week view mockup & 2\\
    15/11 & Architecture draft & 1\\
    21/11 & New event mockup & 2\\
    22/11 & Additional mockups & 1\\
    23/11 & Responsive mockup & 2\\
    24/11 & Event details mockup & 1\\
  \end{tabular}
\end{center}
  
\subsection{Nicolas Darbois}

\begin{center}
  \begin{tabular}{ r l r }
    Date & Task & Hours \\
    \hline
    05/11 & Component view & 2\\
    08/11 & Deployment view & 1\\
    10/11 & Runtime view & 3\\
    15/11 & Component interfaces & 2\\
    21/11 & Implementation and integration and test plan & 2\\
    22/11 & Authentication and login architecture & 3\\
    23/11 & Requirement traceability & 1\\
    24/11 & Correction & 2\\
  \end{tabular}
\end{center}

\end{document}
