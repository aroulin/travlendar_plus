\documentclass[12pt,a4paper]{article}

% Définition des pages
\usepackage[a4paper,margin=1in]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{float}

% Configuration du header des pages
\lhead{Software Engineering}
\rhead{\today}

% Police
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tcolorbox}
%\renewcommand*\rmdefault{bch} % Charter
\renewcommand*\rmdefault{ppl} % Palatino

% Configuration de la table des matières
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}

\begin{document}

	\begin{titlepage}
		\centering
	\vspace{5cm}
		{\scshape\LARGE Software Engineering 2 \par}
	\vspace{1cm}
		{\scshape\Large Travlendar+\par}
	\vspace{1.5cm}
		{\huge\bfseries Requirements Analysis and Specification Document \par}
	\vspace{2cm}
		{\Large \textsc{Nicolas DARBOIS, Antoine ROULIN}\par}
	\vfill
	
		Supervised by\par
		Mr  \textsc{ROSSI}
		\\~
		\\~
		{\large \today\par}
	\end{titlepage}
	
	\newpage
	\tableofcontents
	

% Contenu du rapport %

	\newpage
	\thispagestyle{fancy}
	\section{Introduction}
	
		\subsection{Purpose}
		
The purpose of the present Requirement Analysis and Specification Document (RASD) is to provide a complete description of the system we intend to develop.
We will analyze our users’ needs in order to determine real use cases.
Models of the system will be given, and we will also study the constraints and limits under which we will work.
This document will serve as a guide to the developers during the implementation of the system so that functional and nonfunctional requirements are met.
		
		\subsection{Scope}
		
This project aims to provide flexible and fully-featured calendar support that considers the travel time between meetings. Travlendar+ should support a multitude of travel means, including walking, biking, public transportation, and driving. A particular user may globally activate or deactivate each travel means (e.g., a user who does not own a bicycle would deactivate biking). A user should also be able to provide reasonable constraints on different travel means (e.g., walking distances should be less than a given distance, or public transportation should not be used after a given time of day). When a user interacts with Travlendar, the application should:
		\begin{itemize}
		 \item compute travel times between meetings (or proposed meetings) and prevent scheduling meetings whose start/end times conflict as a result of inability to travel

		 \item explicitly but subtly add "travel time" to the calendar between meetings, reserving the required amount of time on the calendar, and suggesting the "best" option of travel between each meeting.
		 
		 \item create overall compatible schedules on a day-basis, considering that a user who does not leave the house with his bicycle or car in the morning cannot use it to move between meetings during the day.
		\end{itemize}

On a given day, a user's car (or bicycle) should start and end at the user's home. The implementation of Travlendar+ should consider different types of users, including people who work "on the road", traveling from meeting to meeting throughout the day as well as busy parents shuttling kids to and from.

Additional features could be envisioned, a user could specify a flexible “lunch” or other breaks that have to be scheduled and customizable.\\
The calendar could be made dynamic to consider the weather forecast, it could consider the cost of each transportation means, or it could provide users more substantial ability to provide preferences related to transportation modes. 

		
		\subsection{Definitions, Acronyms, Abbreviations}
		
			\subsubsection{Definitions}
			
Appointment: a scheduled meeting containing a list of members, a description of the meeting and the location of this one.
		
			\subsubsection{Acronyms}
			
\begin{itemize}
\item RASD: Requirements Analysis and Specification Document
\item API: Application Programming Interface
\end{itemize}
				
			\subsubsection{Abbreviations}

We won't use any abbreviations to avoid affecting the comprehension of the text.
		
		\subsection{Revision history}
		
\begin{itemize}
  \item 29/10/2017 - 1.0
\end{itemize}
		
		\subsection{Reference Documents}

\begin{itemize}
  \item Specification Document: "Mandatory Projet Assignments.pdf"
  \item Example document : "RASD Sample from A.Y.2016-2017.pdf"
  \item \href{http://homepage.cs.uiowa.edu/~tinelli/classes/181/Spring10/Notes/09-dynamic-models.pdf}{Alloy Dynamic Model example}
  \item \href{http://www.utdallas.edu/~chung/RE/IEEE830-1993.pdf}{IEEE Std 830-1993 - IEEE Guide to Software Requirements Specifications}
  \item \href{http://ieeexplore.ieee.org/document/720574/}{IEEE Std 830-1998 - IEEE Recommended Practice for Software Requirements Specifications}
\end{itemize}
		
		\subsection{Document Structure}
		
The RASD is divided in three different parts.

The first one concerns the description of the product, it focuses on the characteristics of the final product, the stakeholders (users), and the main assumptions and principal dependencies required to build it.

The second part defines precisely what are the technical requirements for the architecture of our system, the constraints, and the basic attributes of it.

Then, in the last part, we present our result obtained with Alloy that we used to formalize the world of our model and to validate our system architecture.

	\newpage
	\thispagestyle{fancy}
	\section{Overall description}	
		
		\subsection{Product perspective}
		
The system will be developed using external APIs in order to estimate the time spent to travel based on the specified means of transport, and the potential constraints related to the weather and the strike days which can directly influence the travel conditions. 
The calendar and all the scheduling system will be developed from scratch to ensure the user gets the best customization. If the time allows us to finish the main requirements in advance, we will add a feature to send invitations to the members of the meeting using our application.\\

\begin{figure}[H]
\centering
\caption{Class diagram}
\includegraphics[scale=0.5]{ClassDiagram1.jpg}
\end{figure}

\begin{figure}[H]
\centering
\caption{Account creation statechart}
\includegraphics[scale=0.6]{AccountCreation.jpg}
\end{figure}

\begin{figure}[H]
\centering
\caption{Travel request statechart}
\includegraphics[scale=0.5]{TravelRequest.jpg}
\end{figure}

\begin{figure}[H]
\centering
\caption{Appointment scheduling statechart}
\includegraphics[scale=0.5]{AppointmentScheduling.jpg}
\end{figure}

		\subsection{Product functions}
		
The main component of the system will be a calendar, allowing the user to visualize its schedule and meetings to come.
The system will allow the user to schedule new meetings, as well as managing existing schedules (adding information, rescheduling, canceling).
The calendar will take into account the workdays of the user (Monday to Friday for instance), and hours during which the user never works.

To these basic functions will be added a travel assistant, in order to help the user with meetings happening in other places than his usual workplace.
The travel time will be estimated and taken into account when a meeting is scheduled.
Different mobility options will also be provided depending on the distance and other user settings:~car, bus, bike…
In the end, once the user has chosen a mean of transport, an itinerary will be saved.

To be as relevant to the user, the system will also regularly check for events like strikes or bad weather, which could influence the time needed to go from one place to another.
Another element taken into account will be breaks like a daily fixed interval for lunch.
Other kinds of breaks could also be set by the user himself.

In fact, many factors will be customizable in order to provide a personalized experience to each user.
Users will be able to choose their favorite transport means, or to ban some, depending on their preferences and possibilities.
They will also be able to put constraints on each mean of transport.
For example, a user could want to walk only if the distance is inferior to 2 kilometers.
Some users would also rather use transport means that have a minimal carbon footprint.

		
		\subsection{User characteristics}
		
Our user target will be pretty broad, because the software will be useful to many kinds of people.
We can assume that our users will not only need to schedule a lot of meetings, but that those meetings will probably be far away from each other.
Indeed, the travel planning function is the main characteristic of our software compared to other calendar or organizational products.

In fact, our users will probably be people who care a lot about optimizing their time and tools if they are interested in a pretty specialized product as the one we will develop.
As such, we can expect they will be ready to spend some time customizing the software so that it fits their needs best.
They should nonetheless have no unnecessary difficulties to adopt the software, and it should as much as possible try to integrate softly in their usual work-flow.
For example, scheduled events could be exported to other solutions like Google Calendar or Apple Calendar which are widely used, especially in mobility.

No assumptions are made about the preferred (or available) means of transport of the user, it will be configurable.
The interface in a general manner should be accessible and add as little difficulties as possible to disabled people.
We also expect our users to speak English, which will be the language used in the application.
		
		\subsection{Assumptions and dependencies}

The credentials that a visitor has to provide to become a registered user are: name, surname, email are essential to complete the registration.\\
The user requires an Internet connection to have access to all the features.
When using our application, the user is always logged in.\\
The details provided by the user such as the means of transport owned must be real in order to get the best travel suggestions.\\
The appointment cannot overlap.\\
Our application relies on APIs that require an Internet connection.
\\
The veracity of the informations sent by the services for the itinerary calculation and other components like weather checking might not be updated or sent to our application. Those issues cannot be corrected by us.

	\newpage	
	\thispagestyle{fancy}
	\section{Specific requirements}
			
		\subsection{External interface requirements}
		
			\subsubsection{User interfaces}

The interface will be centered on a calendar view, which can be displayed in two different modes.
The first will present a monthly calendar, with brief indicators of the presence and number of events for everyday.
The second will present the same kind of information at a weekly scale, allowing for more details (like the location if one is specified).

When a user wants to focus on its current workday or any particular day, he can click on it to make a window pop up presenting even more informations about the events (brief, comments…).

A list of the next events and meetings will also always be present on one side of the application, so that the user can quickly gain access to information about the events to come, like the location, the people involved…

Finally a text bar will serve as quick input to add events using natural language:~“\texttt{meeting with Nicolas in 2 hours}” or “\texttt{conference in Venice next Monday}”.

Precise mock-ups will be provided in the design documents, effectively following those principles.
			
			
			\subsubsection{Hardware interfaces}
			
The only interface to a hardware component the system will presumably need is an interface to some kind of location device.
Modern peripherals such as smart-phones or tablet usually have one. We will access this component on the client side using the Geolocation Web API.
It also works on the desktop, by estimating the position of the user based on its Internet connection.
As long as the user allows it, it should be enough for our needs.

			\subsubsection{Software interfaces}
			
It only requires a web browser to get access to our web-based application.

			\subsubsection{Communication interfaces}
			
One point of interest is that the Geolocation Web API is allowed on the client web browser only if the connection to the website is encrypted (HTTPS).
As a consequence, we will have to ensure it is the case which should not be excessively difficult since secure certificates are now issued freely.
Most hosting providers even deliver them in a automated way.
This secure connection also guarantees a basic security layer for our user and the information he trusts our system to store.

In a similar way, all communications with external services ought to be securely transmitted (cf. \ref{subsubsec:security} Security).

			
		\subsection{Functional requirements}
		
\begin{center}
\includegraphics[scale=0.42,angle=90]{UseCaseDiagram1.jpg}
\end{center}
		
		\subsection{Design constraints}
		
			\subsubsection{Standards compliance}
			
Because the core functionality of our system is managing events and meetings, we will follow the informations taken into account by the iCalendar format which is widely used by all the major calendar applications (Google Calendar, Apple Calendar…).
It will allow our users to export and/or import events from and to our system with relative ease.

For the informations about travel characteristics (length, carbon footprint…) we will not be able to rely on such a universally used standard format.
We will follow the format used by the APIs we will rely on.

			\subsubsection{Hardware limitations}
			
For now we are planning to develop a web application which will require a modern web browser.
By that term we imply that modern Javascript features are supported and enabled, and that we will have access to the Geolocation API.
These are now widely supported by a whole range of browsers on many platforms (desktop and mobile), so we do not expect to let any potential users down.
We can also suppose that our core users have relatively mid to high-end devices that are up to date.
		
		\subsection{Software system attributes}
		
			\subsubsection{Availability, Reliability}

The system should first and foremost work as expected, and of course work as good as possible when there is no network or when external APIs are unreachable.
No assumptions will be made about external services availability in order to stay relevant to the user. A particular focus will be put on data conservation.
Events entered and scheduled by a user should never be lost.
Ideally, the data of a user should also be portable, for example with export and import functions.
			
			\subsubsection{Security}
			\label{subsubsec:security}
			
The system should not disclose the informations entered by its users, given the possibly sensitive of the informations stored:~scheduled meetings and identity of people involved.
The calls to external API should be made in an anonymous way, over an encrypted channel, for the same reasons.
			
			\subsubsection{Maintainability}
			
The system should be maintainable, in a sense that it should be able to evolve in order to follow external API changes for example.
The graphical interface should also be built from reusable components as much as possible, in order to shorten the implementation of new features requiring a graphical display.
It would also help maintaining a sufficient level of stability if a bug fix does not have to be repeated for every part of the interface, because those components are made from the same pieces.
			
			\subsubsection{Portability}
			
The client should be usable on every major desktop system:~Microsoft Windows, GNU\slash Linux and MacOS.
If it is not, a port should not be an impossible task.
In order to achieve this goal, only common libraries will be used to ensure maximum compatibility.
On the other hand, if a server component is developed, its target platform will only be Unix servers.

      \subsubsection{Performance}
      
The system should have a startup time as short as possible to quickly present basic informations about next meetings and the calendar view.
During the creation of a new event, the check for the travel time shouldn't take more than a few seconds (depending on network and connectivity constraints) and should happen in the background in order not to slow the work-flow of the user.

	\newpage
	\thispagestyle{fancy}
	\section{Formal analysis using Alloy}
	
The Alloy allowed us to model a world in which we could formally verify the scheduling of the events.
The model takes into account the duration of the travels for each participant in an event.
Each user has for every every events in which he is involved different means of transports among the ones he chose.
Means of transport are split in two categories: owned by the user or public transportation.

We obtained the following world:

\begin{center}
\includegraphics[scale=0.55]{metamodel.png}
\end{center}

The facts applied to this model ensure:
\begin{enumerate}
  \item events do not overlap when they have participants in common
  \item means of transports available for an event are valid considering user preferences
  \item no itineraries are made for user already on the location
\end{enumerate}

\begin{center}
\includegraphics[scale=0.6]{asserts.png}
\end{center}

\begin{center}
\includegraphics[scale=0.7]{resultsAsserts.png}
\end{center}

These results indicate the facts we applied to the signatures of the types and their relationships constrained their properties enough to ensure the expected behavior of the system.
The signatures and facts can be found in the \texttt{model.als} file in the present directory.
We put here the definition of the predicate \texttt{overlap}:

\begin{center}
\includegraphics[scale=0.65]{predOverlap.png}
\end{center}
	
	\newpage
	\thispagestyle{fancy}
	\section{References}
	
Here we include a list of resources used during the preparation of this document. They helped us to define the nature of the project and its features.
	
\begin{itemize}
  \item \href{https://flexibits.com/fantastical}{Fantastical2}
  \item \href{https://en.wikipedia.org/wiki/ICalendar}{iCalendar}
  \item \href{https://developer.mozilla.org/en-US/docs/Web/API/Geolocation}{Geolocation Web API}
  \item \href{https://en.wikipedia.org/wiki/Carbon_dioxide_equivalent}{Carbone Dioxyde Equivalent}
\end{itemize}

We also collected a few APIs that we will probably use:

\begin{itemize}
  \item \href{https://github.com/saisankargochhayat/carbonfootprint}{Footprint}
  \item \href{https://developers.google.com/maps/documentation/?hl=fr}{Itinerary calculation}
  \item \href{https://api.citybik.es/v2/}{Bike}
  \item \href{https://citymapper.com/api}{Public transportation}
  \item \href{https://developer.uber.com/docs}{Uber}
  \item \href{https://www.taxifarefinder.com/api.php}{Taxi}
  \item \href{https://skyscanner.github.io/slate/?_ga=1.104705984.172843296.1446781555#api-documentation}{Flight}
\end{itemize}

	\newpage
	\thispagestyle{fancy}
	\section{Colophon}
	
	  \subsection{Tools}
	  
\begin{itemize}
  \item Google Drive to gather references
  \item Google Docs to draft the document
  \item \LaTeX~to typeset the document and produce the final pdf file
  \item StarUML to produce diagrams
  \item Alloy 4.2 to produce models of the system
\end{itemize}
	  
 	  \subsection{Time spent}
 	  
The following tables give an indication of the time spent working on the present document, but do not include time spent on research or brainstorming.
 	  
 	    \subsubsection{Antoine Roulin}
 	    
\begin{tabular}{ r l r }
  Date & Task & Hours \\
  \hline
  29/09 & Defining document structure & 1 \\
  04/10 & Starting redaction (overview\ldots) & 2 \\
  05/10 & Overall description & 2 \\
  15/10 & \LaTeX~typesetting & 0.5 \\
  21/10 & Starting requirements & 2 \\
  25/10 & Completing system attributes & 2 \\
  28/10 & Completing Alloy model & 3 \\
  29/10 & Including the Alloy model in the RASD & 3\\
\end{tabular}
 	    
 	    \subsubsection{Nicolas Darbois}
 	    
\begin{tabular}{ r l r }
  Date & Task & Hours \\
  \hline
  29/09 & Defining document structure & 1 \\
  04/10 & Starting redaction (overview\ldots) & 2 \\
  22/10 & Completing parts not done & 3 \\
  28/10 & Creating UML diagrams and charts & 3\\
  29/10 & Including the UML model in the RASD & 1\\
\end{tabular}	
	
\end{document}
