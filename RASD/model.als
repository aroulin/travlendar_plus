open util/time

sig Location, Price, API {}

abstract sig Transport {}

abstract sig PrivateTransport extends Transport {} // transports owned by the user

sig Car extends PrivateTransport {}
sig Bike extends PrivateTransport {}
sig Foot extends PrivateTransport {}

abstract sig ServiceTransport extends Transport { // public transports
	api: API, // custom API for the system to interact with the service
	cost: Price
}

sig Citymapper extends ServiceTransport {}
sig Uber extends ServiceTransport {}
sig BikeMi extends ServiceTransport {}

sig Travel {
	start: Location,
	duration: Time,
	transports: some Transport // possible means of transport
}

sig User {
	owns: set PrivateTransport,
	uses: some Transport // user doesn't want to/can't use some services
}{
	owns in uses
}

sig Event {
	from: Time,
	to: Time,
	place: lone Location,
	members: some User,
	travels: User -> lone Travel // valid iteneraries for each user
}{
	gt[to, from]
	place = none => #travels = none // no iteneraries for events without a location
	#travels <= #members
}

// determines if two events overlap for a user
pred overlap [u: User, e1: Event, e2: Event] {
	lte[e1.from - e1.travels[u].duration, e2.to] && gte[e1.to, e2.from - e2.travels[u].duration]
}

fact {
	// events with members in common cannot overlap (including travel duration)
	all m: User, e1: Event, e2: Event | (m in e1.members & e2.members) => !overlap[m, e1, e2]
	// acceptable transports for an event are the ones that are used by the members
	all e: Event, m: User | m in e.members => m.uses in e.travels[m].transports
	// a travel cannot have for destination the starting point
	all e: Event, m: User | m in e.members => e.travels[m].start != e.place
}

assert noOverlap {
	no u: User, e1:Event, e2:Event | (u in e1.members & e2.members) && overlap[u, e1, e2]
}
//check noOverlap

assert validTransports {
	no e: Event, m: User | m in e.members && !(m.uses in e.travels[m].transports)
}
//check validTransports

assert differentPlace {
	no e: Event, m: User | m in e.members && e.travels[m].start = e.place
}
//check differentPlace
