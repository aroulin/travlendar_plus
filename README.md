# Travlendar+

A travel-time-aware Calendar

- automatically computes and accounts for travel time between appointments to make sure that the user is not late for appointments
- supports the user in his/her travels, for example by identifying the best mobility option, buying public transportation tickets...

## Meeting creation

- warning when when meetings are created at locations that are unreachable in the allocated time
- travel means suggestions depending on the appointment
- takes into account the weather and events like strikes

## User preferences

- travel means choice
- personalized constraints on travel means, like maximum walking distances
- prioritize means of transport that minimize carbon footprint

## Fixed lunch and break hours

- reserve at least half an hour every day between 11:30 and 2:30
- schedule other types of breaks

